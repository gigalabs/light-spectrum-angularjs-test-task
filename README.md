# ChartsApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.0.

## Environment Dependency

node version (18.16.0)

## Installing Angular 15.0.0 CLI

Run `npm i -g @angular/cli@15.0.0`

## Installing Angular 15.0.0 CLI with root permission if normal installation does not work

Run `sudo npm i -g @angular/cli@15.0.0`

## Package installation

Run `npm i`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Usecases Handled

1. Adding a new series to chart.

2. Updating a node.

3. Deleting a node.

4. Moving the node.

5. Gradients from one point to the next point according to the color added for the specific point.

## Unhandled Cases

### Edge cases for point movement that becomes queries.

1. When position of point is moved back from the previous point.

2. When position of point is taken ahead of the next point.
