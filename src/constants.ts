import { IChartData } from './interfaces';

export const timeSlots: string[] = [
  '09:00',
  '10:00',
  '11:00',
  '12:00',
  '13:00',
  '14:00',
  '15:00',
  '16:00',
  '17:00',
  '18:00',
  '19:00',
  '20:00',
  '21:00',
];

export const initialChartData: IChartData[] = [
  {
    title: 'Line 1',
    series: [
      {
        selectedTime: '09:00',
        value: 70,
        color: 'red',
      },
      {
        selectedTime: '10:00',
        value: 30,
        color: 'green',
      },
      {
        selectedTime: '11:00',
        value: 90,
        color: 'blue',
      },
      {
        selectedTime: '13:00',
        value: 40,
        color: 'yellow',
      },
    ],
  },
];
