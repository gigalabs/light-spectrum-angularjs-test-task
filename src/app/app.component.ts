import { Component, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AddSeriesComponent } from './add-series/add-series.component';
import { ChartComponent } from './chart/chart.component';
import { UpdatePointComponent } from './update-point/update-point.component';
import { initialChartData } from 'src/constants';
import {
  IChartData,
  IPointClicked,
  IPointDropped,
  ISeries,
  ISeriesPointIndexes,
  IUpdatePointModalResult,
} from 'src/interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'charts-app';
  public chartData: IChartData[] = [...initialChartData];

  @ViewChild(ChartComponent) chartComponent!: ChartComponent;

  constructor(private dialog: MatDialog) {}

  public addSeries(data: IChartData) {
    if (!!data) {
      this.chartData.push(data);
      this.chartComponent.initChart();
    }
  }

  public openAddSeriesModal() {
    const dialogRef: MatDialogRef<AddSeriesComponent> = this.dialog.open(
      AddSeriesComponent,
      {
        width: '600px',
      }
    );

    dialogRef.afterClosed().subscribe((result: IChartData) => {
      this.addSeries(result);
    });
  }

  public openUpdatePointModal(data: IPointClicked) {
    const { series, point } = data;
    const dialogRef: MatDialogRef<UpdatePointComponent> = this.dialog.open(
      UpdatePointComponent,
      {
        width: '600px',
        data: this.chartData[series].series[point],
      }
    );

    dialogRef.afterClosed().subscribe((result) => {
      if (!!result) {
        switch (result.action) {
          case 'update':
            this.onPointValueChange(series, point, result.data);
            break;
          case 'delete':
            this.deletePoint(series, point);
        }
      }
    });
  }

  public onPointClicked(data: IPointClicked) {
    this.openUpdatePointModal(data);
  }

  private onPointValueChange(
    seriesIndex: number,
    pointIndex: number,
    formData: ISeries
  ) {
    if (!!formData) {
      this.chartData[seriesIndex].series[pointIndex] = formData;
      this.chartComponent.initChart();
    }
  }

  private deletePoint(seriesIndex: number, pointIndex: number) {
    this.chartData[seriesIndex].series = this.chartData[
      seriesIndex
    ].series.filter((_: ISeries, index: number) => index !== pointIndex);
    this.chartComponent.initChart();
  }

  public onPointDropped(data: IPointDropped) {
    const { series, point, valueY, valueX } = data;
    this.chartData[series].series[point].value = valueY;
    this.chartData[series].series[point].selectedTime =
      this.getTimeFromTimestamp(valueX);
    this.chartComponent.updateChart();
  }

  private getTimeFromTimestamp(timestamp: number): string {
    const date: Date = new Date(timestamp);
    let hours: number | string = date.getHours();
    let minutes: number | string = date.getMinutes();
    let ampm: string = '';

    if (hours >= 12) {
      ampm = 'PM';
      hours -= 12;
    } else {
      ampm = 'AM';
    }

    hours = hours === 0 ? 12 : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;

    return `${hours}:${minutes} ${ampm}`;
  }
}
