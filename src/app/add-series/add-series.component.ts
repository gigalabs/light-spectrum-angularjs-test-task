import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

import { timeSlots } from 'src/constants';
import { IChartData } from 'src/interfaces';

@Component({
  selector: 'app-add-series',
  templateUrl: './add-series.component.html',
  styleUrls: ['./add-series.component.scss'],
})
export class AddSeriesComponent {
  constructor(private dialogRef: MatDialogRef<AddSeriesComponent>) {}

  public availableTimeSlots = timeSlots;

  public formData: IChartData = {
    title: '',
    series: [
      {
        selectedTime: '',
        value: 0,
        color: '',
      },
    ],
  };

  closeDialog(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.dialogRef.close(this.formData);
  }

  addTimeValue() {
    this.formData.series.push({
      selectedTime: '',
      value: 0,
      color: '',
    });
  }
  removeTimeValue(indexKey: number) {
    this.formData.series = this.formData.series.filter(
      (_, index) => index !== indexKey
    );
  }
}
