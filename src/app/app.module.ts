import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { AppComponent } from './app.component';
import { ChartComponent } from './chart/chart.component';
import { AddSeriesComponent } from './add-series/add-series.component';
import { UpdatePointComponent } from './update-point/update-point.component';

@NgModule({
  declarations: [
    AppComponent,
    ChartComponent,
    AddSeriesComponent,
    UpdatePointComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
