import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';

import * as Charts from 'highcharts';
import * as HighchartsDraggablePoints from 'highcharts-draggable-points';

HighchartsDraggablePoints(Charts);

import {
  IChartData,
  IChartSeries,
  IPointClicked,
  IPointDropped,
  ISeries,
} from 'src/interfaces';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnChanges {
  @Input() chartData: IChartData[] = [];

  @Output() pointClicked: EventEmitter<IPointClicked> =
    new EventEmitter<IPointClicked>();
  @Output() pointDropped: EventEmitter<IPointDropped> =
    new EventEmitter<IPointDropped>();

  public chartOptions: any = {
    chart: {
      renderTo: 'chart',
      animation: false,
    },
    title: {
      text: 'Time - Intensity Graph',
    },
    xAxis: {
      title: {
        text: 'Time',
      },
      type: 'datetime',
      dateTimeLabelFormats: {
        hour: '%H:%M', // Format for hours (24-hour format)
      },
    },
    yAxis: {
      title: {
        text: 'Intensity',
      },
    },
  };

  public chart: any;

  constructor() {}

  initChart(): void {
    if (this.chart) {
      this.chart.destroy();
    }
    const that = this;
    this.chart = new Charts.Chart({
      ...this.chartOptions,
      plotOptions: {
        series: {
          cursor: 'pointer',
          point: {
            events: {
              click: function (e) {
                e.preventDefault();
                that.onPointClick(this);
              },
              drop: function () {
                // Handle point drop event
                // that.onPointDrop(this);
              },
            },
          },
          draggableX: true,
          draggableY: true,
          draggableYMin: 0,
          stickyTracking: false,
        },
      },
      series: this.generateSeries(),
    });
  }

  ngAfterViewInit(): void {
    this.initChart();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['chartData']) {
      this.updateChart();
    }
  }

  updateChart() {
    if (this.chart) {
      this.chart.update({ series: this.generateSeries() });
    } else {
      this.initChart();
    }
  }

  generateSeries() {
    let series: any = [];

    this.chartData.forEach((item: IChartData) => {
      let seriesObject = {};
      // { label: number; y: number; color: string }
      let seriesData: IChartSeries[] = [];
      const zones: any[] = [];

      item.series.forEach((element: ISeries) => {
        seriesData.push({
          x: this.convertToTimestamp(element.selectedTime),
          y: element.value,
          color: element.color,
        });
      });

      seriesData.sort((a, b) => a.x - b.x);

      seriesData.forEach((data: IChartSeries, index: number) => {
        if (index < seriesData.length - 1) {
          let color: {
            linearGradient: Charts.LinearGradientColorObject;
            stops: [number, string][];
          };
          let linearGradient;
          if (data.y < seriesData[index + 1].y) {
            linearGradient = { x1: 0, y1: 1, x2: 0, y2: 0 };
          } else {
            linearGradient = { x1: 0, y1: 0, x2: 0, y2: 1 };
          }
          color = {
            linearGradient: linearGradient,
            stops: [
              [0, data.color],
              [1, seriesData[index + 1].color],
            ],
          };
          zones.push({
            value: seriesData[index + 1].x,
            color: color,
          });
        }
      });

      seriesObject = {
        ...seriesObject,
        name: item.title,
        data: seriesData,
        cursor: 'move',
        zoneAxis: 'x',
        zones: zones,
        lineWidth: 5,
        type: 'spline',
      };

      series.push(seriesObject);
    });

    return series;
  }
  onPointClick(data: any) {
    this.pointClicked.emit({ series: data.series._i, point: data.index });
  }

  private convertToTimestamp(selectedTime: string): number {
    const date = new Date();
    const timeParts = selectedTime.split(':');
    date.setHours(Number(timeParts[0]));
    date.setMinutes(Number(timeParts[1]));
    date.setSeconds(0);
    date.setMilliseconds(0);
    return date.getTime();
  }

  private onPointDrop(data: any) {
    this.pointDropped.emit({
      series: data.series._i,
      point: data.index,
      valueY: data.y,
      valueX: data.x,
    });
  }
}
