import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { timeSlots } from 'src/constants';
import { ISeries } from 'src/interfaces';

@Component({
  selector: 'app-update-point',
  templateUrl: './update-point.component.html',
  styleUrls: ['./update-point.component.scss'],
})
export class UpdatePointComponent implements OnInit {
  constructor(
    private dialogRef: MatDialogRef<UpdatePointComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ISeries
  ) {}

  public availableTimeSlots = timeSlots;

  public formData = {
    selectedTime: '',
    value: 0,
    color: '',
  };

  ngOnInit(): void {
    if (this.data) {
      this.formData = this.data;
    }
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.dialogRef.close({
      action: 'update',
      data: this.formData,
    });
  }

  removePoint(): void {
    this.dialogRef.close({
      action: 'delete',
    });
  }
}
