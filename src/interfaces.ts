export interface ISeries {
  selectedTime: string;
  value: number;
  color: string;
}

export interface IChartData {
  title: string;
  series: ISeries[];
}

export interface ISeriesPointIndexes {
  series: number;
  point: number;
}

export interface IUpdatePointModalResult {
  action: string;
  data?: ISeries;
}

export interface IChartSeries {
  x: number;
  y: number;
  color: string;
}

export interface IPointDropped {
  series: number;
  point: number;
  valueX: number;
  valueY: number;
}

export interface IPointClicked {
  series: number;
  point: number;
}
